var apn = require('apn');
var config = require('../config');
var fs = require('fs');
var http = require('http');

var models = require('../models');
var config = require('../config');
var mysql      = require('mysql');

var tools = require('../common/tools');

exports.doTask = function(params) {
	
}

exports.newMassTask = function(params) {
	if(params.client_name == null
		|| params.text == null 
		|| params.url == null) {
		
		console.log('传入的参数不正确');
		return false;
	}
	
	var clientName = params.client_name;
	var connection = mysql.createConnection({
		host     : '127.0.0.1',
	    user     : 'root',
		password : '123456',
		database : 'node'
	});

	connection.connect(function(err) {
	  if (err) {
	    console.error('error connecting: ' + err.stack);
	    return;
	  }
	  console.log('mysql connected as id ' + connection.threadId);
	});
	
	connection.query("SELECT count(DISTINCT(`push_token`)) as count, client_name FROM `device` WHERE `platform` != 'android' and ?", {
	    client_name: clientName
	  }, function(err, rows) {
		  var count =  rows[0]['count'];
		  var message = '共找到需要发送的设备' + count + '条';
		  tools.socketLog('data', message);
		  var limit = 100000;
		  var page = parseInt(count / limit + 1);
	  	  
		  for(var i =0; i< page; i++) {
			  var offset = limit * i;
			  connection.query("SELECT DISTINCT `push_token`, client_name FROM `device` WHERE `platform` != 'android' and ? limit " + offset + " ," + limit, {
			      client_name: clientName
			    }, function(err, rows) {
			      var tokens = rows.map(function(row) {
					  var data = {client_name:clientName, text:params.text, url:params.url, token:row.push_token, action:'directOpen', platform:'ios'};
					  var dataString = JSON.stringify(data);
					  //console.log(dataString);
					 global.rabbitMQ.publish(config.pushMassQueue, dataString, {deliveryMode: 2});
					 return row.push_token;
				  });
			  });
		  }
		  
		  var message = count + '个设备发送， 准备完毕';
		  tools.socketLog('data', message);
		  
	  });
	//connection.end();
}

exports.createAPNConnection = function(client_name, callback) {
	
	var apnConnectionOptions = {
		'maxConnections': 100,
		'fastMode': true,
		'passphrase': "qmy110110",
    };
	
	key =  config.keyPath + client_name + '/Production/Key.pem';
    cert = config.keyPath + client_name + '/Production/Cert.pem';
	
	fs.exists(cert, function(exist){
		if(exist) {
		    apnConnectionOptions['key'] = key;
		    apnConnectionOptions['cert'] = cert;
		    apnConnectionOptions['production'] = true;
	
			try {
				var clientAPNConnection = new apn.connection(apnConnectionOptions);
	            clientAPNConnection.on('connected', function() {
	              console.log("Connected");
	            });
	            clientAPNConnection.on('transmitted', function(notification, device) {
	              console.log(client_name + " " + "Notification transmitted to:" + device.token.toString('hex'));
				  tools.socketLog('data', "Notification transmitted to:" + device.token.toString('hex'));
				  if(global.massQueue != null) {
					  global.massQueue.shift();
				  }
				  
	            });
	            clientAPNConnection.on('transmissionError', function(errCode, notification, device) {
	              if (errCode == 8) {
					  if(device != undefined && device.token != undefined) {
						console.log("A error code of 8 indicates that the device token is invalid. " + device.token.toString('hex') );
					  } else {
					  	console.log("A error code of 8 indicates that the device token is invalid. unkonw device");
					  }
	              } else {
	              	console.error("Notification caused error: " + errCode + " for device ", device, notification);
	              }
	            });
	            clientAPNConnection.on('timeout', function() {
	              console.log("Connection Timeout");
	            });
	            clientAPNConnection.on('disconnected', function() {
	              console.log("Disconnected from APNS");
	            });
	            clientAPNConnection.on('socketError', console.error);
				
				
				console.log('new connection2:' + client_name);
				global.APNPool[client_name] = clientAPNConnection;
				
				callback(clientAPNConnection);
			} catch (err) {
				console.log(err);
			}
			
		} else {
		    console.log('[QGL_ERROR] no cert exist, client_name:' + client_name);
			callback(false);
		}
	});
	
}

exports.sendIOS = function(params, callback) {
	if(params == undefined || params.token == undefined || params.text == undefined) {
		return false;
	}
    var note = new apn.Notification();
    note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
    note.badge = 1;
    note.sound = "default";
    note.alert = params.text;
    note.payload.aps = {
      'action': params.action,
      'url': params.url,
    };
    var tokens =  params.token;
	
	var service = null;
	if(global.APNPool[params.client_name] != undefined) {
		service = global.APNPool[params.client_name];
		service.pushNotification(note, tokens);
		
		callback(true);
	} else {
		exports.createAPNConnection(params.client_name, function(r){
			r.pushNotification(note, tokens);
			
			callback(true);
		});
	}
	//return false;
}

exports.sendAndroid = function(params, callback) {
	
	var appkey ='5316a22456240b4c880ebb24';
	var app_master_secret = 'gfenpb5xlpmeloejnlxa6zqg3pgkx5qf';
	var method = 'POST';
	var url = 'http://msg.umeng.com/api/send';
	
	var params = {
  	  	"appkey": appkey,          // 必填 应用唯一标识
		"timestamp":Date.now(),       // 必填 时间戳，10位或者13位均可，时间戳有效期为10分钟
		"type":"unicast",            // 必填 消息发送类型,其值可以为:
		"device_tokens":params.token,   //	可选 设备唯一表示
		"payload":              // 必填 消息内容(Android最大为824B), 包含参数说明如下(JSON格式):
		{
			"display_type":"message",  // 必填 消息类型，值可以为:notification-通知，message-消息
			"body":               // 必填 消息体。display_type=message时,body的内容只需填写custom字段即可。 display_type=notification时, body包含如下参数:                   
			{
				// 通知展现内容:
				"ticker":"通知栏提示文字",     // 必填 通知栏提示文字
				"title":"通知标题",      // 必填 通知标题
				"text":"通知文字描述 ",       // 必填 通知文字描述 
				// 通知到达设备后的提醒方式
				"play_vibrate":"true", // 可选 收到通知是否震动,默认为"true".注意，"true/false"为字符串
				"play_lights":"true",  // 可选 收到通知是否闪灯,默认为"true"
				"play_sound":"true",   // 可选 收到通知是否发出声音,默认为"true"
				// 点击"通知"的后续行为，默认为打开app。
				"after_open": "go_url", // 必填 值可以为:"go_app": 打开应用,"go_url": 跳转到URL, "go_activity": 打开特定的activity， "go_custom": 用户自定义内容。
				"url": params.url,       // 可选 当"after_open"为"go_url"时，必填。
				"custom":"custom"
			},
			extra:                 // 可选 用户自定义key-value。只对"通知(display_type=notification)"生效。 可以配合通知到达后, 打开App, 打开URL, 打开Activity使用。                                  
    		{
			}
		},
		"policy":                // 可选 发送策略
		{
			"start_time":"",   // 可选 定时发送时间，若不填写表示立即发送。定时发送时间不能小于当前时间
			"out_biz_no": ""     // 可选 开发者对消息的唯一标识，服务器会根据这个标识避免重复发送。
		},
		"production_mode":"true", // 可选 正式/测试模式。测试模式下，只会将消息发给测试设备。
		"description": "QGL系统即时消息"      // 可选 发送消息描述，建议填写。
		//"thirdparty_id": "QGL"    // 可选 开发者自定义消息标识ID, 开发者可以为同一批发送的多条消息提供同一个
	}
	http.request({
		host: '',
	})
	
	callback(true);
}

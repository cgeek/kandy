var models = require('../models');
var User = models.User;

var uuid = require('node-uuid');

exports.getUserByUserName = function(username, callback) {
	User.findOne({ username: username}, callback);
}

exports.getUserById = function(user_id, callback) {
	callback({err:false, user:{user_id:1, username:'cgeek'}});
}

exports.newAndSave = function (username, password, email, callback) {
  var user = new User();
  user.username = username;
  user.password = password;
  user.email = email;
  user.accessToken = uuid.v4();
  user.save(callback);
};
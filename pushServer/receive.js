var amqp = require('amqp');
var config = require('./config');
var pushService = require('./services/push2');

global.APNPool = [];
global.massQueue = null;

global.rabbitMQ = amqp.createConnection({host: config.rabbitMQ_host}, {reconnect: true});
global.rabbitMQ.on('ready', function() {
	global.rabbitMQ.queue(config.pushMassQueue, {durable:true, autoDelete: false}, function(queue) {
		//console.log('Subscribe success! Queue:' + config.pushRealtimeQueue);	
		global.massQueue = queue;	
		queue.subscribe({ack:true, prefetchCount:1 }, function(msg) {
			var params = JSON.parse( msg.data.toString('utf-8'));
			if(params.platform == 'ios') {
				try {
					pushService.sendIOS(params, function(r) {
						//if(r) {
							//queue.shift();
						//}
					});
				} catch(e) {
					console.log('push error!!!!');
					console.log(e);
				}
				
			}
			//queue.ack(msg);
			
		});
	});
	
});

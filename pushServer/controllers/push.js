var config = require('../config');
var redis = require('redis');
var pushService = require('../services/push');
var amqp = require('amqp');


exports.newTask = function(req, res) {
	var text = req.body.text,
		url = req.body.url,
		action = req.body.action,
		token = req.body.token,
		client_name = req.body.client_name,
		platform = req.body.platform;

	if( text == undefined || text.length == 0 ||
		url == undefined || url.length == 0 ||
		action == undefined || action.length == 0 ||
		token == undefined || token.length == 0 ||
		client_name == undefined || client_name.length == 0 || 
		platform == undefined || platform.length == 0
	) {
		res.json({success:false, msg : 'params error'});
		return ;
	}
	
	var params = req.body;
	var paramsString = JSON.stringify(params);
	
	var r = global.rabbitMQ.publish(config.pushRealtimeQueue, paramsString, {deliveryMode: 2});
	res.json({success:true});
	/*
	//redis 方案
	if(!global.redisClient) {
		global.redisClient = redis.createClient(config.redis_port, config.redis_host);
	}
	var r = global.redisClient.rpush(config.pushRealtimeQueue, paramsString);
	
	if(r) {
		res.json({success:true});
	} else {
		res.json({success:false});
	}
	*/
}

exports.sendIOS = function(req, res) {
	var text = req.body.text,
		url = req.body.url,
		action = req.body.action,
		token = req.body.token,
		client_name = req.body.client_name;
	
	if( text == undefined || text.length == 0 ||
		url == undefined || url.length == 0 ||
		action == undefined || action.length == 0 ||
		token == undefined || token.length == 0 ||
		client_name == undefined || client_name.length == 0
	) {
		res.json({success:false, 'msg':'params error'});
	} else {
		var params = {client_name:client_name, token:token, action:action, text:text, url:url};
		pushService.sendIOS(params, function(r){
			res.json({success:r});
		});
	}
}


exports.sendAndroid = function(req, res) {
	
	res.json({success:true});
}

exports.testIOS = function(req, res) {
	var text = req.query.text,
		url = req.query.url,
		action = req.query.action,
		token = "",
		client_name = req.query.client_name;
	
		
	text = (text == undefined || text.length == 0) ? '测试' : text;
	url = (url == undefined || url.length == 0) ? 'http://www.qiugonglue.com' : url;
	action = (action == undefined || action.length == 0) ? 'directOpen' : action;
	token = (token == undefined || token.length == 0) ? '451e5c8bc08df20db0ad12f43b683ec3c318f308b1ad88ac96156245cabd2820' : token;
	client_name = (client_name == undefined || client_name.length == 0) ? 'Seoul' : client_name;
	
	var params = {'client_name':client_name, 'token':token, 'action':action, 'text':text, 'url':url};
	pushService.sendIOS(params, function(r){
		res.json({success:r});
	});
	
	
}

exports.testAndroid = function(req, res) {
	
	var text = req.query.text,
		url = req.query.url,
		action = req.query.action,
		token = "",
		client_name = req.query.client_name;
	
	text = (text == undefined || text.length == 0) ? '测试' : text;
	url = (url == undefined || url.length == 0) ? 'http://www.qiugonglue.com' : url;
	action = (action == undefined || action.length == 0) ? 'directOpen' : action;
	token = (token == undefined || token.length == 0) ? 'AvQabRgzNp-1N1I9JyFNNzPP4Rk6rSU9aCZJi5WQt6kO' : token;
	client_name = (client_name == undefined || client_name.length == 0) ? 'Seoul' : client_name;

	var params = {'client_name':client_name, 'token':token, 'action':action, 'text':text, 'url':url};
	
	pushService.sendAndroid(params, function(r){
		res.json({success:r});
	});
}


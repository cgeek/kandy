var UserService = require('../services/user');
var ClientService = require('../services/client');
var config = require('../config');
var auth = require('../middlewares/auth');

var PushService = require('../services/push');

var tools = require('../common/tools');

//控制台
exports.welcome = function (req, res, next) {
	var user = req.session.user;
	
	var data = {};
	if(user) {
		
		data  = {user_id:user.user_id, username: user.user_name}
	}
	res.render('index.html', data);
};

//登录页面
exports.showLogin = function (req, res, next) {
	res.render('login.html');
};

//登录操作
exports.login = function(req, res, next) {
	var username = req.body.username;
	var password = req.body.password;
	
	UserService.getUserByUserName(username, function(err, user) {
		if(!err && user.password == password) {
			auth.gen_session(user, res);
			res.json({success:true});
		} else {
			res.json({success:false, msg:'用户名或者密码错误!'});
		}
	});
}

//退出登录操作
exports.logout = function (req, res, next) {
  req.session.destroy();
  res.clearCookie(config.auth_cookie_name, { path: '/' });
  res.redirect('/dashboard/#/login');
};

exports.clients = function(req, res, next) {
	ClientService.getAllClient(function(err, clients) {
		res.json({client_list:clients});
	});
}

exports.cron_list = function(req, res, next) {
	var cron_list = [
		{id:1, title: '批量发送消息', do_time: '2014-12-24 08:00:00', ctime: '2014-12-23 11:00:00'}
	];
	
	res.json({cron_list: cron_list});
}


exports.init = function(req, res, next)
{
	UserService.newAndSave({username:'cgeek', password:'123456', email:'cgeek@qq.com'}, function(r){
		console.log(r);
	});
	res.json({success:true});
}

exports.newTask = function(req, res, next)
{	
	//console.log(socketLog);
	tools.socketLog('data', '系统通信正常...');
	PushService.newMassTask(req.body);
	res.json({success:true});
}

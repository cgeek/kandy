var moment = require('moment');

// 格式化时间
exports.formatDate = function (date, friendly) {
  date = moment(date);

  if (friendly) {
    return date.fromNow();
  } else {
    return date.format('YYYY-MM-DD HH:mm');
  }
};

exports.socketLog = function(channel, message) {

	if(global.sockets) {
		//var time = exports.formatDate(Date.now());
		var time = moment().format('YYYY-MM-DD HH:mm:ss');
		message = time + '：' + message;
		global.sockets.emit(channel, {message:message});
	}
}

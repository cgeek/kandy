var express = require('express');
var router = express.Router();

var pushController = require('../controllers/push');
var deviceController = require('../controllers/device');
var logController = require('../controllers/log');

/* API */
router.post('/push/new', pushController.newTask);

router.get('/push/testIOS', pushController.testIOS);
router.get('/push/testAndroid', pushController.testAndroid);
//发送push
router.post('/push/sendIOS', pushController.sendIOS);
router.post('/push/sendAndroid', pushController.sendAndroid);

router.post('/device/update', deviceController.update);
//router.post('/log', logController.addLog);

module.exports = router;
var express = require('express');
var router = express.Router();

var dashboard = require('../controllers/dashboard');

router.get('/dashboard', dashboard.welcome);

router.get('/dashboard/clients', dashboard.clients);
router.get('/dashboard/cron_list', dashboard.cron_list);

router.post('/dashboard/newTask', dashboard.newTask);

router.post('/login', dashboard.login);

router.get('/logout', dashboard.logout);

module.exports = router;
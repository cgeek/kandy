var apn = require('apn');
var fs = require('fs');

var key =  './keys/QGLMain/Production/Key.pem';
var cert = './keys/QGLMain/Production/Cert.pem';

var certData = fs.readFileSync(cert, encoding='ascii');
var keyData = fs.readFileSync(key, encoding='ascii');
console.log(keyData);
var options = {
	    'passphrase': "qmy110110",
		"production"     : true,
		        "key" : keyData,
				        "cert" : certData,
						        "maxConnections" : 5,
								"interval"       : 300,

};

var apnConnection = new apn.Connection(options);

var note = new apn.Notification();

note.expiry = Math.floor(Date.now() / 1000) + 3600;
note.badge = 3;
note.sound = "ping.aiff";
note.alert = "\uD83D\uDCE7 \u2709 You have a new message";
note.payload = {'messageFrom': 'Caroline'};

var deviceToken = 'a6fe5f2cd0c2d91e60ca3705140a350dd96192af17446b262656fc26084b18a4';
var myDevice = new apn.Device(deviceToken);


apnConnection.pushNotification(note, myDevice);

console.log('start to process token');

var models = require('./models');
var Device = models.Device;

var mysql      = require('mysql');
var connection = mysql.createConnection({
	host     : '127.0.0.1',
    user     : 'root',
	password : '123456',
	database : 'merchant'
});

connection.connect(function(err) {
  if (err) {
    console.error('error connecting: ' + err.stack);
    return;
  }
  console.log('mysql connected as id ' + connection.threadId);
});


connection.query("SELECT * FROM `device` WHERE 1", function(err, rows) {
	rows.map(function(row) {
	  console.log(row.push_token, row.platform, row.client_name, row.client_version);
      //return row.push_token;
	  newDevice(row);
	});
});

function newDevice(data, callback) {
    var device = new Device();
    device.uuid = data.uuid;
    device.platform = data.platform;
    device.push_token = data.push_token;
    device.user_id = data.user_id;
    device.client_name = data.client_name;
    device.client_version = data.client_version;
    device.os_version = data.os_version;
	device.screen_size = data.screen_size;
	device.last_open_time = data.last_open_time;
    device.save(callback);
}
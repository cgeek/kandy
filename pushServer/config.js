var config = {
	debug : true,
	session_secret: 'session_secret',
	auth_cookie_name: 'auth_cookie_name',
	port: 8898,
	keyPath : __dirname + '/keys/',
	pushRealtimeQueue: 'push_realtime_queue',
	pushMassQueue: 'push_mass_queue',
	
    // mongodb 配置
    mongodb: 'mongodb://127.0.0.1/qgl',
	redis_port: 6379,
	redis_host: '127.0.0.1',
	
	rabbitMQ_host: 'localhost'
	
}

module.exports = config;
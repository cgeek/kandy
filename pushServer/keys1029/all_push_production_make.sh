#!/usr/bin/bash
for d in *
do
  if [ -d ${d} ]; then
    cd $d/Production
    if [ ! -f ck.pem ]; then
      bash ~/bin/push_make.sh -p
    fi

    cd ../Development

    if [ ! -f ck.pem ]; then
      bash ~/bin/push_make.sh -d
    fi

    
    cd ../..
  fi
done

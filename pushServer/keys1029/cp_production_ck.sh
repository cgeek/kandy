#!/bin/bash

PRODUCTION_DIR=Production

TARGET_DIR="ProductionCKs"

for clientName in *
do
  if [ -d $clientName ]
  then
    originFilePath="${clientName}/${PRODUCTION_DIR}/ck.pem"
    if [ -e $originFilePath ]
    then 
      desFilePath="${TARGET_DIR}/ck_${clientName}.pem"
      cp "${originFilePath}" "${desFilePath}"
    fi
  fi
done

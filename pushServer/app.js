var config = require('./config');

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var bodyParser = require('body-parser');

var ejs = require('ejs');
var http = require('http');
var fs = require('fs');

require('./models');

var app = express();

var redis   = require('redis');
var RedisStore = require('connect-redis')(session);

app.use(session({
    store: new RedisStore({host:config.redis_host, port:config.redis_port}),
    secret: config.session_secret,
	resave: true,
	saveUninitialized: true
}));

// view engine setup
app.set('views', path.join(__dirname, 'web'));
//app.set('view engine', 'ejs');
app.engine('.html', ejs.__express);
app.set('view engine', 'html');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'web')));


var io = require('socket.io');

var cronJob = require('cron').CronJob

var redisServer = redis.createClient(config.redis_port, config.redis_host);

var pushService = require('./services/push');

global.redisClient = redis.createClient(config.redis_port, config.redis_host);

//apn和苹果服务器的连接池
global.APNPool = {};

var watchList = [];
var server = http.createServer(app);
var sockets = io.listen(server);
sockets.on('connection', function(socket) { 
    //socket.emit('data', watchList);
});

global.sockets = sockets;

var auth = require('./middlewares/auth');
app.use(auth.auth_user);
app.use(auth.auth_api);

var apiRoutes = require('./routes/api');
var webRoutes = require('./routes/web');

app.use('/api', apiRoutes);
app.use('/', webRoutes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});


if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
	});
}

app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

/*
var sub = function() {
    pubSubClient.subscribe(config.pushRealtimeQueue);
};
sub();

pubSubClient.on('error', function(error) {
    console.log(error);
    sub();
});
pubSubClient.on('message', function(channel, value) {
	console.log("Handling message: " + value);
	if(channel == config.pushRealtimeQueue && value!=null) {
		var params = JSON.parse(value);
		if(params.platform == 'ios') {
			pushService.sendIOS(params, function(r){
				console.log(value);
				//io.sockets.emit(config.pushRealtimeQueue,value);
			});
		} else if(params.platform == 'android') {
			pushService.sendAndroid(params, function(r){
				//io.sockets.emit(config.pushRealtimeQueue,value);
				console.log(r);
			});
		}
	}
	console.log("Handling pmessage: " + channel);
	
});
*/
/*
setInterval(function() {
	//每秒轮询pop消息队列
    redisServer.rpop(config.pushRealtimeQueue, function (err,value) {

        if(value!=null)
        {
			var params = JSON.parse(value);
			if(params.platform == 'ios') {
				pushService.sendIOS(params, function(r){
					console.log(value);
					//io.sockets.emit(config.pushRealtimeQueue,value);
				});
			} else if(params.platform == 'android') {
				pushService.sendAndroid(params, function(r){
					//io.sockets.emit(config.pushRealtimeQueue,value);
					console.log(r);
				});
			}
			
        }
    });

},1000);
*/

//
// new cronJob('* * * * * *', function() {
//
// 	//Send the update to the clients
//     sockets.emit('data', {time:Date.now()});
// }, null, true);


server.listen(config.port, function(){
	console.log("Server listening on port %d in %s mode", config.port, app.settings.env);
});
module.exports = app;


/*
var MQSClient = require('aliyun_mqs');

global.mqsClient = new MQSClient({
	accessKeyId: 'oxuY9uJsk0CMOFFr',
  	accessKeySecret: 'nTcWnMIbjnvRsNonuD4XslPg3uzIde',
    url: 'http://cmbw4r7l5o.mqs-cn-beijing.aliyuncs.com'
});
*/

var amqp = require('amqp');
global.rabbitMQ = amqp.createConnection({host: config.rabbitMQ_host});
global.rabbitMQ.on('ready', function() {
	//console.log('RabbitMQ is ready!');
	global.rabbitMQ.queue(config.pushRealtimeQueue,  {durable:true, autoDelete: false}, function(queue) {
		//console.log('Subscribe success! Queue:' + config.pushRealtimeQueue);
		
		queue.subscribe(function(msg) {
			console.log("Received %s", msg.data.toString('utf-8'));
			var params = JSON.parse( msg.data.toString('utf-8'));
			if(params.platform == 'ios') {
				pushService.sendIOS(params, function(r){
					console.log(r);
				});
			} else if(params.platform == 'android') {
				pushService.sendAndroid(params, function(r){
					console.log(r);
					
				});
			}
		});
		
	});
	
	
	global.rabbitMQ.queue(config.pushMassQueue, {durable:true, autoDelete: false}, function(queue) {
		//console.log('Subscribe success! Queue:' + config.pushRealtimeQueue);		
		/*
		queue.subscribe(function(msg) {
			var params = JSON.parse( msg.data.toString('utf-8'));
			if(params.platform == 'ios') {
				//pushService.sendIOS(params, function(r){});
			}
			//console.log("Received %s", msg.data.toString('utf-8'));
		});
		*/
	});

});

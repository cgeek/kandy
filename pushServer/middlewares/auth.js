var config = require('../config');
var userService = require('../services/user');

function gen_session(user, res) {
	var auth_token = user.user_id + '$$$$'; // 以后可能会存储更多信息，用 $$$$ 来分隔
	
	var r = res.cookie(config.auth_cookie_name, auth_token, {path: '/', maxAge: 1000 * 60 * 60 * 24 * 30, httpOnly:true}); //cookie 有效期30天
}

exports.gen_session = gen_session;

exports.auth_user = function(req, res, next) {
	if(req.path.indexOf('dashboard') < 0) {
		return next();
	}
	
	if(req.session.user) {
		return next();
	} else {
		var auth_token = req.cookies[config.auth_cookie_name];
		if(!auth_token) {
			return next();
		}
		
	    var auth = auth_token.split('$$$$');
	    var user_id = auth[0];
		
		userService.getUserById(user_id, function(r) {
			req.session.user = r.user;
			next();
		});
	}
}

exports.auth_api = function(req, res, next) {
	if(req.path.indexOf('api') < 0) {
		return next();
	}
	
	if(!req.query.token || req.query.token != 'abc') {
		res.send('auth failed');
	} else {
		next();
	}
}
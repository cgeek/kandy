var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ClientSchema = new Schema({
	  client_name: { type: String, unique:true },
	  client_name_cn: { type: String },
});

ClientSchema.index({client_name: 1}, {unique: true});

mongoose.model('Client', ClientSchema);
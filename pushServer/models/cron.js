var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CronSchema = new Schema({
	  title: { type: String, unique:true },
	  desc: { type: String },
});

CronSchema.index({client_name: 1}, {unique: true});

mongoose.model('Cron', CronSchema);
	
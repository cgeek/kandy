var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
	  username: { type: String, index:true },
	  password: { type: String },
	  email: { type: String, unique:true },
      appKey: {type:String, unique:true},
	  accessToken: {type: String, unique:true}
});

UserSchema.index({username: 1}, {unique: true});
UserSchema.index({accessToken: 1});

mongoose.model('User', UserSchema);

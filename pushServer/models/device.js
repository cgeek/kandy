var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var DeviceSchema = new Schema({
	platform: { type: String, index:true},
	uuid: { type: String, index:true},  
	push_token: { type: String },
	user_id: { type: String, index:true},
	client_name: { type: String },
	client_version: { type: String },
	os_version: { type: String },
	screen_size: { type: String },
	ctime: { type: String },
	last_open_time: { type: String },
});

DeviceSchema.index({client_name: 1});

mongoose.model('Device', DeviceSchema);

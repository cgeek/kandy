var mongoose = require('mongoose');
var config = require('../config');

mongoose.connect(config.mongodb, function (err) {
  if (err) {
    console.error('connect to %s error: ', config.mongodb, err.message);
    process.exit(1);
  }
});

// models
require('./user');
require('./client');
require('./device');

exports.User = mongoose.model('User');
exports.Client = mongoose.model('Client');
exports.Device = mongoose.model('Device');

angular.module('app.controllers', [])
.controller('LoginCtrl', function($scope, $http, $location) { 
	$scope.submit = function() {
		$http.post('/login', $scope.formData).success(function(result){
			if(result.success == true) {
				$location.path("/");
			}
		});
	}
})

.controller('ClientCtrl', function($scope, $http) {
	$http.get('/dashboard/clients').success(function(r) {
		$scope.client_list = r.client_list;
	});
	
})

.controller('PushCtrl', function($scope, $http, socket) {
	$http.get('/dashboard/clients').success(function(r) {
		$scope.client_list = r.client_list;
	});
	
	$scope.submit = function() {
		$http.post('/dashboard/newTask', $scope.task).success(function(r){
			//console.log(r);
		});
	}
	
	$scope.messages = [];
    //var socket = io.connect(window.location.hostname);
    socket.on('data', function(data) {

		$scope.messages.push(data.message);
		//console.log($scope.messages);
		//$scope.time = data.time;
    });
    // socket.on('data', function(data) {
// 			console.log(data);
//     });
	
	
})

.controller('CronCtrl', function($scope, $http) {
	$http.get('/dashboard/cron_list').success(function(r) {
		$scope.cron_list = r.cron_list;
	});
	
})
.controller('WelcomeCtrl', function($scope, $http){ 
	console.log('welcome');
});


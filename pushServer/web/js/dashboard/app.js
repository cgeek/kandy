'use strict';

var app = angular.module('app', ['ngRoute', 'app.controllers']);

app.factory('socket', function($rootScope) {
	var socket = io.connect(window.location.hostname);

	return {
		on: function(eventName, callback) {
			socket.on(eventName, function() {
				var args = arguments;
				$rootScope.$apply(function() {
					callback.apply(socket, args);
				});
			});
		},
		emit: function(eventName, data, callback) {
			socket.emit(eventName, data, function() {
				var args = arguments;
				$rootScope.$apply(function() {
					if(callback) {
						callback.apply(socket, args);
					}
				});
			})
		}
	};
	
});


app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $routeProvider
        .when('/', {templateUrl: '/views/tpl/welcome.html', controller: 'WelcomeCtrl'})
		.when('/login', {templateUrl: '/views/tpl/login.html', controller: 'LoginCtrl'})
		.when('/client', {templateUrl: '/views/tpl/client.html', controller: 'ClientCtrl'})
		.when('/push', {templateUrl: '/views/tpl/push.html', controller: 'PushCtrl'})
		.when('/cron', {templateUrl: '/views/tpl/cron.html', controller: 'CronCtrl'})
        .otherwise({redirectTo: '/'});
    $locationProvider.html5Mode(false);
}]);

